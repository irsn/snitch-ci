#!/bin/bash

rm -f **/*_{ui,rc}.py
rm -rf **/__pycache__
rm -rf dist build snitch.egg-info

