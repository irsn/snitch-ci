#!/bin/bash

# starting X server
sudo -Eu xserver Xvfb $DISPLAY -screen 0 ${SCREEN_SIZE}x24 &

echo " >>> DISPLAY=$DISPLAY"
echo " >>> PROGRAM_NAME=$PROGRAM_NAME"
echo " >>> ID=$(id)"
echo " >>> OUTPUT_PATH=$OUTPUT_PATH"
ps aux | grep X
ls -al /tmp
env

# using xdpyinfo to check if X server ready
until xdpyinfo 2>&1 > /dev/null
do
    echo Display not ready
    sleep 1
done

# controlling the window size and position

win_id=''
while [[ -z $win_id ]] #until xwininfo -tree -root
do
    echo " >>> Looking for window of program '$PROGRAM_NAME'"
    win_id=$(xwininfo -tree -root | grep -i "$PROGRAM_NAME" | sed -e 's/^[[:space:]]*//' | cut -d' ' -f 1 | tail -n1 )
    xwininfo -tree -root
    if [[ -z $win_id ]]
    then
        echo Application not ready
        sleep 1
    fi
done
##win_id=$(wmctrl -lp | grep -i xeyes | head -n1 | cut -d' ' -f1)
echo " >>> WINDOW ID=$win_id"

if [[ ! -z $win_id ]]
then
    # resizing window to full screen and positioning in the top left corner
    xdotool windowmove $win_id 0 0  windowsize $win_id '100%' '100%'
    chown xserver:xserver /caps
    echo " >>> ROOT dir:"
    ls -l /
    echo " >>> OUT dir:"
    ls -l $OUTPUT_PATH

    DIR=$OUTPUT_PATH
    mkdir -p $DIR
    chown xserver:xserver $DIR
    FILE=$DIR/capture_$(date '+%s').png
    sudo -Eu xserver touch $FILE
    ls -l /caps
    #import -display $DISPLAY -window root $FILE
    sudo -Eu xserver scrot $FILE
    file $FILE

else
    echo "ERR: Window not found."
    exit 1
fi
