#!/bin/bash
echo " <<< DISPLAY=$DISPLAY"
echo " <<< PROGRAM_NAME=$PROGRAM_NAME"
env
ls -l /

# waiting for display
until xdpyinfo 2>&1 > /dev/null
do
    echo Display not ready
    sleep 1
done


$PROGRAM_NAME

