FROM registry.gitlab.com/irsn/snitch-ci/python/pyqt5

RUN apt-get update
RUN apt-get install -y sudo

# creating user to share X11 socket
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/xserver && \
    echo "xserver:x:${uid}:${gid}:xserver,,,:/home/xserver:/bin/bash" >> /etc/passwd && \
    echo "xserver:x:${uid}:" >> /etc/group && \
    mkdir -p /etc/sudoers.d && \
    echo "xserver ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/xserver && \
    chmod 0440 /etc/sudoers.d/xserver && \
    chown ${uid}:${gid} -R /home/xserver

RUN mkdir /tmp/.X11-unix && chmod 1777 /tmp/.X11-unix
RUN mkdir /caps && chmod 777 /caps

ENV DISPLAY :99

# starting xserver
CMD sudo -Eu xserver Xvfb $DISPLAY -screen 0 1920x1080x24 &
