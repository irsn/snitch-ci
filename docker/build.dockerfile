FROM python:3.7

# Python dev tools
RUN apt-get update
RUN apt-get install -y qttools5-dev-tools qt5-default
RUN apt-get install -y tesseract-ocr tesseract-ocr-fra

RUN apt-get install -y git

COPY requirements.txt ./
RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt
