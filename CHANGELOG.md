Snitch CI version history
=========================


## 0.7.2 - 2020-08-25

### Fixed
-   Crash when running a test case with no expected result
-   Recent files menu
-   Crash if capture area is empty (same origin an final point)
### Changed
-   Defaulting to stderr for log output
-   Mouse position on minified view
-   Adding tolerance on double click detection
-   Inserting new event after the selected one (was inserting before)
-   Minimizing UI to make space for screenshots


## 0.7.1 - 2020-04-17

### Fixed
-   Freeze on non-interactive replay
### Changed
-   Default window size and toolbar names

## 0.7.0 - 2020-04-15

### Added
-   CLI option to specify output directory for test results
-   Full implementation of the « minimize on record » option
-   Rearranging buttons as toolbars


## 0.6.0 - 2020-04-02

### Added
-   OCR functionality


## 0.5.3 - 2020-03-12

### Added
-   Keyboard event flowchart
-   Confirmation dialog on clear
-   Comment event
### Changed
-   Minor cosmetic changes on events list
### Fixed
-   Keyboard events management (fixes key inversions and key used with
    modifiers)
-   Space and Backspace are now properly detected as TextEntered events


## 0.5.2 - 2020-02-28

### Added
-   Automatic release creation and deployment on tag
-   Adding CHANGELOG.md to repository
### Fixed
-   Screenshot delete, move up and move down button are now operational
-   KeyEventCatcher no longer listens to keyboard after replay sequence
    completion when _Delay_ events are used
-   Automatic release description


## 0.5.1 - 2020-02-20

### Fixed
-   Crash when replayed event has no kill method
-   Crash when using the `-f` option on command line


## 0.5.0 - 2020-02-18

### Added
-   New testcase file management
-   Deselecting last event after playback
-   Release automated on tag
### Changed
-   Escape key is now enabled while processing Delay
### Fixed
-   Snapshot preview properly updated on deselection
-   Selection frame not captured anymore
