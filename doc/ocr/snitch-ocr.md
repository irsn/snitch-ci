Snitch, implémentation d'une fonctionnalité OCR
===============================================

## Expression du besoin

L’outil Snitch-CI permet d’enregistrer des actions utilisateur sous forme de
séquences. Ces séquences peuvent par la suite être rejouées de manière
automatique pour former des tests d’interface graphique. Pour déterminer si un
test est passé avec succès, une capture d’écran de la zone d’intérêt est
réalisée à la fin de l’enregistrement de celui-ci. Une capture de la même zone
de l’écran est réalisée quand le test est rejoué de manière automatique, et les
deux images sont comparées. Si elles sont identiques le test est considéré comme
validé.

Cette technique fonctionne très bien, si l’environnement logiciel est
strictement identique entre l’enregistrement du test et sa réalisation. Mais un
changement de version de système d’exploitation, de framework logiciel (par
exemple de version de machine virtuelle Java) peut créer de légères différences
d’affichage, en particulier des textes (lissage des polices, …) qui invalident à
tort le résultat des tests.

Fixer un seuil de tolérance inférieur à 100% n’est pas une solution : selon les
tests effectués, afin de faire passer des tests valides dont seul le rendu est
différent, il faut abaisser le seuil en dessous de celui ou certains tests
invalides ne devraient plus passer.

Il est donc envisagé de recourir à la reconnaissance de caractères, et à la
comparaison des résultats textuels pour pallier ce problème de rendu des textes.

## Choix technologique

Le choix du programme de reconnaissance de caractères (OCR) s’est naturellement
porté sur Tesseract. Publié sous licence libre Apache, c’est un programme
soutenu par Google, qui possède une API en Python, pyTesseract, utilisable
directement par Snitch.

Les premiers tests d’implémentation se sont révélés plutôt décevants, Tesseract
n’arrivant pas à reconnaître les textes de l’interface du logiciel Latec. Des
recherches dans la documentation ont montré que pour que Tesseract fonctionne
correctement, il faut que les images en entrée subissent un prétraitement. C’est
tout l’objet du document [Finding the best OCR
method](snitch-ocr-preprocessing.md), pour lequel plus de 250 combinaisons de
paramètres ont été testés afin de trouver les plus adéquats à notre problème.

## Implémentation

### Interface graphique

L’interface de Snitch est modifiée pour ajouter les nouvelles fonctionnalités de
reconnaissance de caractère afin que l’utilisateur puisse :

-   Avoir un aperçu de l’interprétation de l’image par le programme ;
-   Spécifier le texte effectivement attendu ;
-   Voir le texte de la capture de résultat ;
-   De manière optionnelle spécifier un ensemble de caractères parmi lesquels la
    reconnaissance de caractères piochera. Cela permet de limiter certaines
    erreurs (par exemple en ne spécifiant que les chiffres de 0 à 9, la
    confusion entre 1, l, et I n’a plus lieu d’être).

Les changements d’interface sont mis en évidence par la capture d’écran
suivante :

![OCR options](snitch_ui_ocr_options.png)

La capture suivante montre que bien que les images de référence et après
exécution du test soient différentes (zones cerclées de rouge), la
reconnaissance de caractères produit le même résultat textuel, ce qui permet de
valider le test, ce qui n’aurait pas été le cas avec la comparaison d’image
seule.

![Diff screen](snitch_ui_diff.png)

### Mode ligne de commande

En mode ligne de commande, le mode utilisé lors de la lecture des tests de
manière automatique, la manière de déterminer le succès ou l’échec d’un test est
également modifiée.

Il a été choisi que la reconnaissance de caractères ait la précédence sur la
comparaison d’image seule. Donc, si un test ne possède pas d’attendu textuel,
c’est la comparaison d’image qui détermine le succès ou l’échec du test, en
revanche si un texte est attendu, la reconnaissance de caractères prime. Dans
tous les cas le détail de chaque comparaison est imprimé en sortie du programme
de manière à être parsé facilement par les scripts appelants. L’exemple suivant
montre la sortie de l’exécution d’un test pour lequel sept captures d’écran sont
effectuées. On constate que :

-   Les deux premières et la dernière passent la comparaison d’image (les autres
    échouent) ;
-   Seules les trois dernières sont configurées pour effectuer une
    reconnaissance de caractères ;
-   Parmi ces trois dernières, deux passent l’OCR avec succès, la dernière
    échoue ;
-   Le bilan est donc que trois captures d’écran (la 3, la 4 et la 7) ne passent
    pas pour ce cas test.

```bash
$ python3 -m snitch -f test_ocr.json

Loading test case : test_ocr.json
Playing test case...

IMG> ++----+
OCR> ....++-

Summary:
  - 3/7 matching screenshots
  - 2/3 matching texts
Saving results...

$ echo $?
3
```