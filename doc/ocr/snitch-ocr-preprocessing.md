Finding the best OCR method
===========================

## Preamble

_This paragraph is widely ispired by “[How you can get started with Tesseract](https://medium.com/free-code-camp/getting-started-with-tesseract-part-i-2a6a6b1cf75e)”._


### OCR

We’ll be using the Tesseract optical character recognition engine.

In itself it has not much options to performs the recognition, merely a “lang”
parameter to specify the language the text submitted is written in. But feeding
Tesseract with a raw image is likely to give poor results. For the algorithm to
work one have to prepare the input image to make it more “readable”.


### Image preprocessing

#### Rescaling
Tesseract works best on large images. If you’re working with relatively small
images, you might consider rescaling.

#### Noise Removal
Most printed documents are likely to experience noise to some extent. Though the
main reasons for this noise may vary, it’s clear that it makes it harder for
computers to recognize characters. Noise on images can be removed by using a few
different techniques combined. These include but are not limited to converting
image to grayscale, dilation, erosion and blurring.

#### Binarization
This is definitely a must. Let’s try to think like a computer for a second. In
this cyber-reality where everything eventually boils down to 1’s and 0’s,
converting image to black and white immensely helps Tesseract recognize
characters. However, this might fail if input documents lack contrast or have a
slightly darker background.


## Procedure

The base image has been choosen to be typical of a test case result: it contains
tabulated data, elements of graphical interface, small numeric characters…

![base image](3D_geometry_variables_definition.png)

Batches of OCR have been run with various pa

### Parameters

-   scale factor: 1, 2, 3, 4
-   threshold function: threshold, adaptiveThreshold
-   threshold type: binary, tozero, binary + otsu, tozero + otsu
-   blur method: median, gaussian
-   blur size: 9, 7, 5, 3, none


### Results

Most of the preprocessing parameters sets gave unusable images, either blank, or
with not enough (or too much) contrast, or too blury, or even some that are
humanly readable but that Tesseract was unable to process.

Here are some examples:

![blank](3D_geometry_variables_definition_x1/3D_geometry_variables_definition_x1_threshold_median_7_tozero.png)

![blacked out](3D_geometry_variables_definition_x2_linear/3D_geometry_variables_definition_x2_threshold_gaussian_5_tozero_otsu.png)

![human readable](3D_geometry_variables_definition_x4/3D_geometry_variables_definition_x4_threshold_median_5_binary_otsu.png)



Finally the best results were obtained with the following set of parameters:

  Scale | Function  | Blur method       | Binarization
  -----:|-----------|-------------------|---------------
  2x    | threshold | gaussian blur 3px | to_zero
  2x    | threshold | gaussian blur 3px | to_zero + otsu
  2x    | threshold | gaussian blur 5px | to_zero + otsu
  2x    | threshold | no blur           | to_zero
  2x    | threshold | no blur           | binary + otsu
  2x    | threshold | no blur           | to_zero + otsu
  2x    | threshold | median blur 3px   | to_zero + otsu
  4x    | adaptive  | gaussian blur 3px | binary

Here “best” is defined by text results with the minimum number of mismatched
characters compared to the original image.

With those results, other parameters have been tested:

-   Using the __erode/dilate__ method seem to have no effect at all on the
    output images (since it’s screenshots, there is no noise to eliminate);
-   Adding « __equ__ » as OCR language doesn’t change anything on the best
    results (the recognized characters differ on the worst resulst, but are
    equally wrong);
-   The __cubic__ interpolation is found to be slightly better than the
    __linear__ one for image enlargment: 0s, 6s and 8s are more easily
    distinguished.

#### Linear
![best result](3D_geometry_variables_definition_x2_linear/3D_geometry_variables_definition_x2_threshold_none_0_tozero_otsu.png)
```
N | var.h_u (real | var.m_u (real | varr_sphere (...| var.c_u (real) KEFF COMBI. GENERALE ECART TYPE KEFF+30 f
1 | 1 28000.0 19.814057921...|7071.695816... 0.22123 0.00100 0.22423 cS
2 | 5 28000.0 12.51398857... |3411.021447... 0.52014 0.00100 0.52314
3 | 7 28000.0 13.51191382... |2709.685090... 0.60686 0.00100 0.60986
4 | 10 28000.0 14.77855101... |2070.970232... 0.69462 0.00100 0.69762
5 | 12 28000.0 15.51532766... |1789.720078... 0.73556 0.00100 0.73856
6 | 15 28000.0 18.50444517... |1486.848209... 0.78002 0.00100 0.78302
7 | 20 28000.0 17.92955520... |1159.740038... 0.82640 0.00100 0.8294
8 | 25 28000.0 19.15828818... |950.6005724... 0.85020 0.00100 0.8532
9 | 30 28000.0 120.24883501... |805.3747145... 0.86183 0.00100 0.86483
10 | 40 28000.0 122.12868801... |616.8824637... 0.86589 0.00100 0.86889
```

#### Cubic
![best result](3D_geometry_variables_definition_x2_cubic/3D_geometry_variables_definition_x2_threshold_none_0_tozero_otsu.png)
```
N | var.h_u (real) | var.m_u (real | varr_sphere (....) var.c_u (real) KEFF COMBI. GENERALE ECART TYPE KEFF+20
1 | 4 28000.0 = /9.814057921...|7071.695816... 0.22123 0.00100 0.22423
2 | 5 28000.0 = |12.51396657... |3411.021447... 0.52014 0.00100 0.52314
3 | 7 28000.0 = |13.51191382... |2709.685090... 0.60686 0.00100 0.60986
4 | 10 28000.0 = /14.77855101... |2070.970232... 0.69462 0.00100 0.69762
5 | 12 28000.0 = |15.51532766... |1789.726078... 0.73556 0.00100 0.73856
6 | 15 28000.0 = |16.50444517... |1486.848209... 0.78002 0.00100 0.78302
7 | 20 28000.0 =| 17.92955520... |1159.740636... 0.82640 0.00100 0.8294
8 | 25 28000.0 = |19.15826618... |950.6065724... 0.85020 0.00100 0.8532
9 | 30 28000.0 = |20.24683501... |805.3747145... 0.86183 0.00100 0.86483
10 | 40 28000.0 = |22. 12868801... |616.8824637... 0.86589 0.00100 0.86889
```
#### Highlighted diff
![linear vs. cubic](linear_vs_cubic.png)
