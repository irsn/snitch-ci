Keyboard events
===============


```mermaid
graph TD
  E((Waiting for Event))

  KP[KeyPress]
  M{is modifier?}
  MKP{are modifier<br>key pressed?}
  TS[Trigger shortcut]
  PC{is printable<br>character?}
  TT[Trigger text]
  TK[Trigger key]

  E -.-> KP
  KP --> M
  M -- no --> MKP
  M -- yes --> E
  MKP -- no --> PC
  MKP -- yes --> TS
  MKP -- yes, but it's<br> `shift` or `alt_gr`<br> and it produces a<br> printable character --> TT
  PC -- yes --> TT
  PC -- no --> TK

  KR[KeyRelease]
  E -.-> KR
  MM{is modifier?}
  U{is unique?}

  KR --> MM
  MM -- no -->  E
  MM -- yes --> U
  U -- yes --> TK
  U -- no --> E
```