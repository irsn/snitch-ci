# Guide de l'utilisateur de l'outil d'enregistrement des actions utilisateurs SNITCH

Ce document présente le fonctionnement du logiciel SNITCH ainsi que l’utilité de chaque fonctionnalité. Il s’agit d’un logiciel d’enregistrement des actions utilisateurs. Chaque action réalisée par l’utilisateur, d’une entrée au clavier jusqu’au clic de souris, sera enregistrée par le logiciel et pourra être rejouée si besoin est. La Figure 1 présente l’interface du logiciel.

![center](user_guide.fig1.main_interface.png)

Figure 1 : Interface du logiciel SNITCH

Il est important de garder à l’esprit que les clics sont enregistrés à l’aide de « coordonnées » sous forme de numéros de pixel. Une « macro » enregistrée à l’aide d’un écran dont la résolution est 1920 * 1080 ne pourra pas être jouée sur un écran de résolution différente. De plus, la session de l’utilisateur souhaitant jouer à nouveau une « macro » SNITCH devra être dans la même configuration que celle utilisée lors de l’enregistrement. Pour l’enregistrement de jeu de tests sur l’outil LATEC, la fenêtre doit être en plein écran, aucun projet ne doit être ouvert et le fichier « .latec » se trouvant dans le dossier utilisateur doit être supprimé de manière à obtenir la fenêtre graphique de l’outil LATEC dans la configuration d’origine utilisée lors des enregistrements. Le fonctionnement du logiciel est détaillé ci-après sachant que chaque sous-section correspond à un numéro de la Figure 1.

## L’enregistrement d’une « macro »

Le bouton « RECORD » permet de lancer l’enregistrement d’une « macro ». À partir du moment où l’utilisateur actionne cette commande, le logiciel enregistre toutes les actions effectuées. La commande « Stop » permet l’arrêt de l’enregistrement.

Lors de l’enregistrement d’un jeu de tests pour l’outil LATEC, ce dernier est d’abord ouvert en plein écran suivi du logiciel SNITCH. L’enregistrement est lancé et la première action est un « clic » sur la fenêtre de l’outil LATEC de manière à faire disparaître le logiciel SNITCH et ne garder que la fenêtre de l’outil LATEC. Le projet peut alors être créé. Lorsque toutes les commandes ont été effectuées, SNITCH doit être ouvert à nouveau au niveau de la barre des tâches avant d’actionner le bouton « Stop ». La dernière action enregistrée, le clic au niveau de la barre des tâches, doit donc être supprimé. Cela sera détaillé dans le § 2.3.

## Commandes enregistrées

Ce paragraphe s’intéresse à la gestion des actions enregistrées par l’utilisateur.

### Liste des actions

La zone 2.1 de la Figure 1 correspond à la liste des actions réalisées par l’utilisateur et qui ont été enregistrées par le logiciel SNITCH. Elles sont rangées dans l’ordre de réalisation des différentes commandes. Il y est indiqué l’heure et la date (dans le langage de l’ordinateur) auxquelles s’est jouée l’action, le type de commande effectuée ainsi que les détails associés.

### Les différentes actions réalisables

Huit actions sont enregistrables à l’heure actuelle avec le logiciel SNITCH :

-   L’action « MouseClick » est un clic de souris caractérisé par une abscisse x et une ordonnée y qui sont associées à un pixel de l’écran de l’utilisateur ;

-   Les actions « MouseDoubleClick » et « MouseAltClick » fonctionnent de la manière que l’action « MouseClick » et réalisent respectivement un double-clic et un clic droit ;

-   L’action « MouseDrag » correspond à un « drag and drop » et est caractérisée par des jeux de coordonnées. Le déplacement s’effectue du point (x, y) vers le point (xx, yy) ;

-   « TextEntered » est un texte entré au clavier. Il prend en compte toutes les lettres majuscules et minuscules ainsi que les chiffres et les symboles ;

-   L’action « KeyPressed » correspond à l’actionnement d’une touche du clavier en particulier. Parmi celles les plus utilisées se trouvent :
	-   la clef « entrée » : « enter » dans SNITCH,
	-   la clef « retour » pour effacer du texte : « backspace » dans SNITCH,
	-   la clef « supprimer » : « delete » dans SNITCH,
	-   la clef « F2 » : « f2 » dans SNITCH ;

-   Les commandes de type « ShortCut » servent aux commandes associant deux touches du clavier comme « Maj+J ». Les quatre raccourcis disponibles sont « ctrl », « shift », « alt » et « alt_g  ». A ces raccourcis sont associées des commandes au clavier. Par exemple, pour écrire un « a » majuscule, il faut que le type de raccourci soit « shift » auquel est associé la lettre « a ». Il est également possible d’entrer directement « A » dans la commande « TextEntered ». Ces raccourcis doivent être utilisés avec précaution. Par exemple, si un utilisateur souhaite entrer un « a » majuscule dans une case et actionner ensuite la commande « enter » pour valider la case, la commande « enter » sera prise en compte dans le raccourci « shift » si les deux actions sont réalisées trop rapidement. Il est donc conseillé de laisser un court laps de temps entre la commande « shift+a » et la commande « enter » dans cet exemple ;

-   La dernière commande est « Delay ». Entre deux actions enregistrées avec le logiciel SNITCH s’écoule un cours laps de temps qui est constant. Si une pause est requise entre deux actions, par exemple entre le lancement d’un calcul et l’ouverture des résultats, un délai peut être ajouté. L’action « Delay » se caractérise par une durée en ms ;

### Organisation des actions réalisées

La zone 2.3 de la Figure 1 permet d’organiser les actions enregistrées par le logiciel SNITCH. Le bouton en forme de croix verte permet d’ajouter une action. Pour cela, il faut au préalable sélectionner une commande dans la liste des actions déjà enregistrées. La nouvelle action se situera avant celle sélectionnée. Un menu déroulant s’affichera et l’utilisateur pourra sélectionner l’action qu’il souhaite ajouter parmi celles présentées au § 2.2.

La croix rouge permet de supprimer une action sélectionnée dans la liste des événements enregistrés. Elle sera très utile pour supprimer le dernier « click » permettant d’afficher la fenêtre du logiciel SNITCH dans le but d’arrêter l’enregistrement comme expliqué au § 1.

Les deux flèches sur la droite permettent de descendre ou de monter une action dans la liste des événements enregistrés.

Pour terminer cette sous-section sur les commandes enregistrées, il est important de souligner qu’une fois un jeu d’événements enregistré, il est possible d’ajouter plusieurs actions à cette liste à l’aide d’un nouvel enregistrement. Pour cela, l’utilisateur devra sélectionner l’action à la suite de laquelle il souhaite enregistrer les nouvelles commandes. Lorsque chaque nouvelle action aura été enregistrée, l’utilisateur devra revenir sur le logiciel SNITCH et arrêter l’enregistrement. Comme expliqué au § 1, le « click » permettant de revenir à SNITCH devra lui aussi être supprimé.

## L’ajout d’une capture d’écran

L’objectif de ces jeux de tests est d’amener l’outil LATEC dans une configuration souhaitée, typiquement avec l’affiche des résultats d’un calcul, de manière à pouvoir comparer les résultats obtenus lorsque la « macro » sera rejouée avec la nouvelle version et ceux obtenus lors de l’enregistrement avec la version stable de l’outil. C’est cette comparaison qui fait office de test. A la fin de l’enregistrement de la « macro » amenant à un résultat de calcul, une capture d’écran de la fenêtre de résultats est réalisée pour sauvegarder les résultats obtenus avec la version stable de l’outil LATEC.

### La capture

Cette capture d’écran s’effectue en actionnant la commande « Add result ». L’utilisateur devra alors sélectionner une région de son écran qui fera office de comparatif entre les versions. La capture s’affiche alors dans la liste sous la commande « Add result » qui détaille toutes les captures enregistrées. Généralement, une seule capture de la zone de résultat sera présente.

### Caractéristiques de la capture

La capture d’écran est caractérisée par une miniature, par un jeu de coordonnées (x, y) indiquant l’origine de la zone capturée, située en haut à gauche de l’image, et par sa taille.

### Organisation des captures d’écran

Comme cela est expliqué au § 2.3 pour les actions enregistrées, il est possible de réorganiser les différentes captures réalisées à l’aide des mêmes commandes décrites.

## Test des nouvelles versions de l’outil latec et enregistrement d’une « macro »

L’objectif de ces « macros » SNITCH est la réalisation des commandes enregistrées avec une version stable de l’outil LATEC pour une nouvelle version. Une fois les commandes jouées, la capture d’écran est réalisée et est comparée à celle prise lors l’établissement de la « macro ». Si les deux diffèrent, une mauvaise manipulation lors de la création de la nouvelle version de l’outil LATEC est à envisager.

Afin de rejouer une « macro » SNITCH, il suffit de sélectionner la commande à partir de laquelle on souhaite répéter les actions enregistrées et actionner la commande « Play ». À partir de cet instant, le logiciel SNITCH rejouera chaque action effectuée par l’utilisateur. La séquence d’actions rejouées peut être stoppée à tout moment en actionnant la commande « Echap ». De retour sur le logiciel SNITCH, la prochaine commande qui aurait été réalisée au moment de l’arrêt est sélectionnée dans la listes des actions enregistrées.

La commande « Save » permet de sauvegarder le projet SNITCH au format .json et la commande « Clear » supprime toutes les actions enregistrées ainsi que les captures d’écran.

## Guide des bonnes pratiques

Ce paragraphe liste de manière aussi exhaustive que possible les difficultés auxquelles pourrait être confronté un utilisateur.

Comme expliqué au § 2.2, il est fortement conseillé de prendre son temps lors de l’enregistrement des actions notamment lors de l’enchaînement de deux actions impliquant au moins pour l’une l’utilisation d’un raccourci. De manière générale, il est conseillé d’utiliser le moins possible les raccourcis.

Entre deux clics, le curseur de la souris se dirige en ligne droite entre les deux points. Lors de l’utilisation d’un menu déroulant, notamment lors de la création d’une loi de dilution, cela peut mener à la fermeture de la liste des options si le curseur ne reste pas sur cette même liste.

![100% center](user_guide.fig2.sub_menus.png)

Figure 2 : Création d'une loi de dilution UPuO2 avec l'outil LATEC

La Figure 2 présente la façon de créer une loi de dilution UPuO2. Lors d’une utilisation de l’outil LATEC sans le logiciel SNITCH, un utilisateur effectuera un clic droit sur « Materials » puis naviguera son curseur de souris jusqu’à trouver « UPuO2 » sur lequel il effectuera un clic pour créer la loi de dilution. Si l’utilisateur effectue cela lors de l’enregistrement avec SNITCH, le curseur se déplacera en ligne droite depuis l’action « AltClick » sur « Materials » jusqu’au « MouseClick » sur « UPuO2 » comme le montre le trait rouge sur la Figure 2. Le curseur ne passera donc pas sur les options « Dilution laws » ni même sur « UPu ». D’autres actions sont donc à réaliser entre temps. Après la commande « MouseAltClick » sur « Materials », l’utilisateur devra cliquer sur « Dilution laws » de manière à ouvrir la liste des lois de dilution. Ensuite, s’il clique directement sur « UP  » le curseur ira en ligne droite et risque de sortir de la zone graphique « Dilution laws » avant de passer dans la liste des lois de dilution entraînant la fermeture de cette dernière. Après avoir sélectionné « Dilution laws », l’utilisateur devra alors déplacer sa souris sur « U » et effectuer un « MouseDrag » de « U » vers « UPu de manière à ne jamais sortir des listes affichées. De la même manière, il devra enfin réaliser un drag and drop de « UPumetal » à « UPuO2 ». Cette méthode est à appliquer dans d’autres situations comme la création d’un volume par exemple.


Concernant l’entrée de texte dans une case comme le nom ou la légende d’une variable ou encore une dimension pour une géométrie, il est conseillé de cliquer sur la case en question puis d’actionner la commande « f2 » afin d’entrer dans la case à remplir. Effectuer un double-clic ne fonctionne pas toujours, notamment après l’ouverture d’une nouvelle fenêtre.


