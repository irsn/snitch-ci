Snitch
======


## Architecture overview

```mermaid
graph TD
  subgraph Picocs GitLab-Runner host
    D(Docker daemon)
    D -. "/var/run/docker.sock" .-> C
    FS(Filesystem)
    FS -. "/builds/…" .-> C
    FS -."/builds/…" .-> S
    subgraph GitLab-Runner container
      C[docker-compose]
      C --> S
      C --> P
      subgraph Tested program container
        P[Tested program]
        X11 -.-> P
      end
      subgraph Snitch container
        X11(Xvfb) -. /tmp/.X11-unix .-> S[Snitch]
      end
    end
  end
```


## File structure

This section presents the different configuration files used for Snitch to work in the continuous integration chain, and the order in which each one is used. For each file


```mermaid
graph TD
  A[.gitlab-ci.yml] --> B[docker-compose.yml]
  B --> C
  B --> D
  B --> D2
  subgraph Tested program
    subgraph Program
      D[Dockerfile] --> DD[run.sh]
    end
    subgraph Services
      D2[Dockerfile]
    end
  end
  subgraph Snitch
    C[Dockerfile] --> CC[snitch.sh]
  end
```

The entry point is the GitLab CI configuration file, it defines the environment and runs Docker compose. Docker compose starts the containers used for the tests, one for Snitch, one for the tested program. The latter may require other containers to be started, in which case they also need to be described in the Docker compose configuration file.

The Snitch Dockerfile is pre-configured and the image is available at [gitlab.com/irsn/snitch:latest](). It basically just prepares the environment to execute Snitch, sets up a virtual X11 server to act as deported display for the tested program and runs the script that will collect the test cases, run the test and write the test report.

On the other hand it's up to the program developer to write the Dockerfile for the tested program. A template is [provided further on this document](#program-container). It's role is to provide the dependencies, environment and entry point to the program.

### CI configuration (.gitlab-ci.yml)

This file prepares the environment, and calls `docker-compose` to start the containers and run the tests.

To pass a variable from the continuous integration configuration level to the execution level, the following steps must be followed:

  - the variable must be exported in the script secton of the job defined in `.gitlab-ci.yml`;
  - the variable must be redefined in the `docker-compose.yml` for each container using it;
  - the variable can be used by the programs running in the container, but can't be used in the build steps of the `Dockerfile`: docker-compose is a runtime tool that assumes the image has been previously built (i.e. with no knowledge of the variables).

The following snippet is an example of a working file.

```yaml
stages:
  - run

image:
  name: docker/compose:1.23.2
  entrypoint: ["/bin/sh", "-c"]

before_script:
  - docker version
  - docker-compose version

run_compose:
  stage: run
  variables:
    SHARED_PATH: ${CI_PROJECT_DIR}/shared
    OUTPUT_PATH: ${CI_PROJECT_DIR}/shared/${CI_BUILD_REF}

  script:
    - export OUTPUT_PATH=${CI_PROJECT_DIR}/shared/${CI_BUILD_REF}
    - mkdir -p ${OUTPUT_PATH}
    - docker-compose down -v
    - docker-compose build --build-arg HTTP_PROXY=$PROXY
    - docker-compose up

  artifacts:
    paths:
      - ${OUTPUT_PATH}/*
```

Things to take care of and caveats to avoid when writing your own configuration:

  - the version of the docker/compose image must be hard coded, there is no “latest” tag;
  - the entrypoint must be used a is;
  - the variable `$OUTPUT_PATH` must be declared in the `variables` section in order to be used in the `artifacts` section;
  - the `$CI_PROJECT_DIR` is used to be able to use `$OUTPUT_PATH` for the artifact, as artifacts can only reference file from inside the project directory;
  - the `$CI_BUILD_REF` is used to isolate one run from another and not mix up the files;
  - the `$OUTPUT_PATH` variable must be exported in the `script` section to make it available from inside the `docker-compose.yml`;
  - there is some issues with the use of the variables declared in the `variables` section from within the `script` section, so the export is fully written;
  - on the call of `docker-compose down` the `-v` must not be omitted. It deletes the volumes shared between the containers, and it's important not to have remaining from previous runs.


### Docker compose (docker-compose.yml)

```yaml
version: "3"
services:
  snitch:
    image: snitch:latest
    environment:
      - DISPLAY=${DISPLAY}
      - PROGRAM_NAME=${PROGRAM_NAME}
      - SCREEN_SIZE=${SCREEN_SIZE}
      - OUTPUT_PATH=${OUTPUT_PATH}
    volumes:
      - ${OUTPUT_PATH}:${OUTPUT_PATH}
      - xsocket:/tmp/.X11-unix

  xtools:
    image: xtools:latest
    environment:
      - DISPLAY=${DISPLAY}
      - PROGRAM_NAME=${PROGRAM_NAME}
      - SCREEN_SIZE=${SCREEN_SIZE}
    volumes:
      - xsocket:/tmp/.X11-unix

volumes:
  xsocket:
```

### Snitch container

The Snitch container shares the test case directory and the result directoy with the program container. By default the test case directory is `BASE/tests` and the result directory under  `BASE/tests/results`, where “BASE” is the project directory where GitLab cloned the project.




### Program container

#### Dockerfile

```docker
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y sudo
RUN apt-get install -y x11-utils

# creating user to share X11 socket
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/xserver && \
    echo "xserver:x:${uid}:${gid}:xserver,,,:/home/xserver:/bin/bash" >> /etc/passwd && \
    echo "xserver:x:${uid}:" >> /etc/group && \
    mkdir -p /etc/sudoers.d && \
    echo "xserver ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/xserver && \
    chmod 0440 /etc/sudoers.d/xserver && \
    chown ${uid}:${gid} -R /home/xserver


RUN mkdir /tmp/.X11-unix && chmod 1777 /tmp/.X11-unix
COPY xtools.sh ./

CMD sudo -Eu xserver ./xtools.sh
```

#### Execution script

It is highly recommended to wait for the script to wait for the X11 server provided by the Snitch container. This can be achieved with the following code, placed at the beginning of the script.

```bash
# waiting for display
until xdpyinfo 2>&1 > /dev/null
do
    echo Display not ready
    sleep 1
done
```

The `xdpyinfo` command is provided on Ubuntu by the “x11-utils” package installed in the Dockerfile.


### Runner configuration (/etc/gitlab-runner/config.toml)

Be careful how the runners are declared on registration. They must be registered with the following options, if they don't, the `/etc/gitlab-runner/config.toml` config file must be edited manually afterwards:

```bash
    gitlab-runner register --non-interactive \
      --url http://gitlab.com \
      --registration-token $TOKEN \
      --locked=false \
      --executor "docker" \
      --description "docker-runner-ID" \
      --docker-image "docker:latest" \
      --docker-privileged \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
      --docker-volumes /builds:/builds
```

  - __docker-privileged:__ to be allowed to run docker commands from within the container;
  - __docker-volumes:__

    - `/var/run/docker.sock`: the socket to be able to use the host docker daemon from within the container;
    - `/builds`: the directory to get the test results;

  - __locked==false__: make the runners available right after the registration.


As an example here's an entry from the `config.toml` file created by the aforementioned command:

```toml
[[runners]]
  name = "docker-runner-ID"
  url = "http://gitlab.com"
  token = "edHWQDt5-LsG4duRwnD1"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/produits/sec:/produits/sec", "/irsn:/irsn", "/builds:/builds", "/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```
