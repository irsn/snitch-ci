<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="">
<context>
    <name>Controller</name>
    <message>
        <location filename="../ui/controller.ui" line="309"/>
        <source>Record</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="321"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="330"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="144"/>
        <source>Play</source>
        <translation>Rejouer</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="203"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="274"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="282"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="297"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../ui/controller.py" line="20"/>
        <source>{0} events recorded.</source>
        <translation>{0} événements enregistrés.</translation>
    </message>
    <message>
        <location filename="../ui/controller.py" line="42"/>
        <source>Event added: {0}</source>
        <translation>Événement ajouté : {0}</translation>
    </message>
    <message>
        <location filename="../ui/controller.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/controller.ui" line="335"/>
        <source>Save…</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/controller.ui" line="292"/>
        <source>About…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventListModel</name>
    <message>
        <location filename="../model/event.py" line="79"/>
        <source>Time</source>
        <translation>Horodatage</translation>
    </message>
    <message>
        <location filename="../model/event.py" line="80"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../model/event.py" line="81"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
</context>
</TS>
