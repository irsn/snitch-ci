#!/bin/bash

for ui in *.ui
do
    echo "Uic'ing $ui..."
    pyuic5 "$ui" -o "${ui/.ui/_ui.py}" --import-from=snitch
done
